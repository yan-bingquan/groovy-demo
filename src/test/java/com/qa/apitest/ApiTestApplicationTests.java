package com.qa.apitest;

import com.qa.apitest.web.TestController;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;

@SpringBootTest
class ApiTestApplicationTests {
    TestController testController =new TestController();
    @Test
    void contextLoads() {
        Object result=testController.testSend();
        System.out.println(result.toString());
    }
    @Test
    void mkdir() throws IOException {
        File file = new File("logs");
        FileUtils.writeStringToFile(file,"dagt", "UTF-8", true);
    }

}
