package com.qa.apitest.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping("log")
public class TemplatesController {

    @RequestMapping("/index")
    public String demo3(Model model,@RequestParam String fileName) throws IOException {
        List list = new LinkedList();
        String dirName = "logs";
        String errorLogFileName = dirName + "/" + fileName;
        File filename = new File(errorLogFileName); // 要读取以上路径的input。txt文件
        InputStreamReader reader = new InputStreamReader(
                new FileInputStream(filename)); // 建立一个输入流对象reader
        BufferedReader br = new BufferedReader(reader); // 建立一个对象，它把文件内容转成计算机能读懂的语言
        String line = null;
        line = br.readLine();
        while (null != line) {
            line = br.readLine(); // 一次读入一行数据
//            list=list+"\r\n"+line;
            list.add(line);
        }
        System.out.println(list);
        model.addAttribute("level",list);
        return "welcome";
    }

}