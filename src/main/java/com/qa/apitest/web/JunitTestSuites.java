package com.qa.apitest.web; /**
 * @program: qncq-api-test
 * @description:
 * @author: yanbingquan
 * @create: 2019-11-21 09:47
 **/

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import com.qa.apitest.web.groovy.TestTest;

@RunWith(Suite.class)
@SuiteClasses({TestTest.class})
public class JunitTestSuites {
}