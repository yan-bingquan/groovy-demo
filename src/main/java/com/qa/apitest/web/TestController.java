package com.qa.apitest.web;

import com.alibaba.fastjson.JSONObject;
import com.qa.apitest.service.LogService;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RestController
public class TestController {
    /**
     * 接口测试
     */
    @GetMapping("/api/test/send")
    public Object testSend() {
//        Result result = JUnitCore.runClasses(JunitTestSuites.class);
        ParalleTest paralleTest =new ParalleTest();
        Result result = paralleTest.testRunner();
        List<String> failureDescription = new LinkedList<>();
        long time = System.currentTimeMillis();
        LogService.createLog(time, result);

        Map MessageMap = new HashMap<>();
        MessageMap.put("runTestCount", result.getRunCount());
        MessageMap.put("runFaileureCount", result.getFailureCount());
        MessageMap.put("runIgnoreCount", result.getIgnoreCount());
        MessageMap.put("RunTime", result.getRunTime());
        MessageMap.put("result", result.wasSuccessful());
        MessageMap.put("link","http://localhost:8182/log/index?fileName=log_error_"+time+".txt");
        if (0 < result.getFailureCount()) {
            for (Failure failure : result.getFailures()) {
                failureDescription.add(failure.getDescription().getDisplayName());
            }
            MessageMap.put("runFaileCaseName", failureDescription);
        }
        return JSONObject.toJSON(MessageMap);
    }
}
