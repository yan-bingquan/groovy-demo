package com.qa.apitest.web;

import com.qa.apitest.web.groovy.TestTest;
import com.qa.apitest.web.groovy.TestTesttest;
import org.junit.experimental.ParallelComputer;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

/**
 * @program: qncq-api-test
 * @description: 并发
 * @author: yanbingquan
 * @create: 2020-05-31 15:03
 **/
public class ParalleTest {
    public Result testRunner() {
        Class[] cls = {TestTest.class, TestTest.class};
        Result result;

        //并发以类为维度，AB两个类串行执行，但类的方法还是串行执行；
        result = JUnitCore.runClasses(ParallelComputer.classes(), cls);

//        //并发以方法为维度，A，B两个类串行执行，但是类的方法并发执行
//        result = JUnitCore.runClasses(ParallelComputer.classes(), cls);
//
//        //并发以类和方法为维度，A，B 两个类并发执行，其方法也并发执行
//        result = JUnitCore.runClasses(new ParallelComputer(true, true), cls);

        return result;
    }


    public static void main(String[] args) {
        Class[] cls = {TestTest.class, TestTesttest.class};
        Result result;

        //并发以类为维度，AB两个类串行执行，类的方法还是串行执行；
//        result = JUnitCore.runClasses(ParallelComputer.methods(), cls);

        //并发以方法为维度，A，B两个类串行执行，但是类的方法并发执行
//        result = JUnitCore.runClasses(ParallelComputer.classes(), cls);

        //并发以类和方法为维度，A，B 两个类并发执行，其方法也并发执行
        result = JUnitCore.runClasses(new ParallelComputer(true, true), cls);

        System.out.println(" "+result.getRunCount() + "" + result.getFailures() + "" + result.getRunTime());
    }
}