package com.qa.apitest.web.groovy


import spock.lang.Specification

class TestTesttest extends Specification {
//    def "获取短信验证码 /auth/vCode"() {
//        given:
//        HttpClientUtils httpClientUtils = new HttpClientUtils()
//        when:
//        Map<String, String> params = new HashMap<>()
//        params.put("mobile", "17601017253")
//        def result = httpClientUtils.doGet(Confs.env + "/auth/vCode", params).content
//        println result
//        def code = JSON.parse(result).get("code")
//        then:
//        assert code == 0
//    }
//
//    def "登录 auth/login"() {
//        given:
//        HttpClientUtils httpClientUtils = new HttpClientUtils()
//        when:
//
//        Map hearerMap = new QncqCommonParams().createHeader()
//        Map<String, String> params = new HashMap<>()
//        params.put("phone", "17601017253")
//        params.put("code", "1111")
//
//        String result = httpClientUtils.httpPostWithJson(Confs.env + "/auth/login", hearerMap, new JSONObject(params).toString())
//        println result
//        def code = JSON.parse(result).get("code")
//        then:
//        assert code == 0
//
//    }
//
//    def "获取所有的任务信息 /api/mission/findAll"() {
//        given:
//        HttpClientUtils httpClientUtils = new HttpClientUtils()
//        when:
//        //登录获取token
//        Map hearerMap = new QncqCommonParams().createHeader()
//        Map<String, String> params = new HashMap<>()
//        params.put("phone", "17601017253")
//        params.put("code", "1111")
//        String tokenResult = httpClientUtils.httpPostWithJson(Confs.env + "/auth/login", hearerMap, new JSONObject(params).toString())
//        def token = JSON.parse(tokenResult).get("data").get("token")
//        //保存任务
//        Map<String, String> saveParams = new HashMap<>()
//        saveParams.put("missionId", "1")
//        Map<String, String> findHearerMap = new HashMap()
//        findHearerMap.put("Authorization", "Bearer " + token)
//        String saveResult = httpClientUtils.doPost(Confs.env + "/api/mission/save", findHearerMap, saveParams).content
//        println saveResult
//        //查询任务
//        def result = httpClientUtils.doGet(Confs.env + "/api/mission/findAll", findHearerMap, null).content
//        println result
//        def code = JSON.parse(result).get("code")
//        def missionId = JSON.parse(result).get("data")[1].get("missionId")
//        def isFinish = JSON.parse(result).get("data")[1].get("isFinish")
//        then:
//        assert code == 0
//        assert missionId == 2
//        assert isFinish == 1
//    }
//
//    def "获取用户打卡签到记录 /api/usersignature/findAll"() {
//        given: "获取token"
//        HttpClientUtils httpClientUtils = new HttpClientUtils()
//        def token = QncqCommonParams.getToken("17601017253", "1111")
//        Map<String, String> findAllHearerMap = new HashMap()
//        findAllHearerMap.put("Authorization", "Bearer " + token)
//
//        when:
//        //打卡
//        String saveResult = httpClientUtils.doPost(Confs.env + "/api/usersignature/save", findAllHearerMap, null).content
//        println saveResult
//        //查看打卡记录并校验
//        def time = DateUtil.getTime()
//        def result = httpClientUtils.doGet(Confs.env + "/api/usersignature/findAll", findAllHearerMap, null).content
//        println result
//        def code = JSON.parse(result).get("code")
//        ArrayList data = JSON.parse(result).get("data").get("signatures")
//        for (signature in data) {
//            def signatureTime = signature.get("signatureTime")
//            if (time == signatureTime) {
//                assert signature.get("isSignature") == true
//            }
//        }
//        then:
//        assert code == 0
//    }
//
//    def "获取已经打卡人数 /api/usersignature/findSignatureNumbers"() {
//        given:
//        HttpClientUtils httpClientUtils = new HttpClientUtils()
//        when:
//        def result = httpClientUtils.doGet(Confs.env + "/api/usersignature/findSignatureNumbers", null).content
//        println result
//        def code = JSON.parse(result).get("code")
//        def signatureNumbers = JSON.parse(result).get("data").get("signatureNumbers")
//        then:
//        assert code == 0
//        assert 100 <= signatureNumbers
//    }
//
//    def "分页获取积分排行，默认前十名 /api/points/findAll"() {
//        given:
//        HttpClientUtils httpClientUtils = new HttpClientUtils()
//        when:
//        Map<String, String> params = new HashMap<>()
//        params.put("pageNumber", "1")
//        params.put("pageSize", "10")
//        def result = httpClientUtils.doGet(Confs.env + "/api/points/findAll", params).content
//        println result
//        def code = JSON.parse(result).get("code")
//        then:
//        assert code == 0
//    }
//
//    def "分页获取课程专辑列表 /api/courseAlbum/findAll"() {
//        given:
//        HttpClientUtils httpClientUtils = new HttpClientUtils()
//        when:
//        Map<String, String> params = new HashMap<>()
//        params.put("pageNumber", "1")
//        params.put("pageSize", "10")
//        def result = httpClientUtils.doGet(Confs.env + "/api/courseAlbum/findAll", params).content
//        println result
//        def code = JSON.parse(result).get("code")
//        then:
//        assert code == 0
//    }
//
//    def "根据课程专辑标题模糊查询 /api/courseAlbum/findByTitleContaining"() {
//        given:
//        HttpClientUtils httpClientUtils = new HttpClientUtils()
//        when:
//        Map<String, String> params = new HashMap<>()
//        params.put("pageNumber", "1")
//        params.put("pageSize", "10")
//        params.put("title", "计算机")
//        def result = httpClientUtils.doGet(Confs.env + "/api/courseAlbum/findByTitleContaining", params).content
//        println result
//        def code = JSON.parse(result).get("code")
//        then:
//        assert code == 0
//    }
//
//    def "根据ID获取课程专辑中课程列表  /api/course/findByAlbumId/{albumId}"() {
//        given:
//        HttpClientUtils httpClientUtils = new HttpClientUtils()
//        def albumId = 1
//        when:
//        def result = httpClientUtils.doGet(Confs.env + "/api/course/findByAlbumId/" + albumId, null).content
//        println result
//        def code = JSON.parse(result).get("code")
//        then:
//        assert code == 0
//    }
//
//    def "保存APP用户已学习的课程 /api/course/saveUserStudyCourse"() {
//        given:
//        HttpClientUtils httpClientUtils = new HttpClientUtils()
//        when:
//        //登录获取token
//        def token = QncqCommonParams.getToken("17601017253", "1111")
//        //保存任务
//        Map<String, String> saveParams = new HashMap<>()
//        saveParams.put("courseId", "1")
//        Map<String, String> findHearerMap = new HashMap()
//        findHearerMap.put("Authorization", "Bearer " + token)
//        String saveResult = httpClientUtils.doPost(Confs.env + "/api/course/saveUserStudyCourse", findHearerMap, saveParams).content
//        println saveResult
//        //查询任务
//        def result = httpClientUtils.doGet(Confs.env + "/api/course/findByAlbumId/1", findHearerMap, null).content
//        println result
//        def code = JSON.parse(result).get("code")
//        def id = JSON.parse(result).get("data")[0].get("id")
//        def isFinish = JSON.parse(result).get("data")[0].get("isFinish")
//        then:
//        assert code == 0
//        assert id == 1
//        assert isFinish == "1"
//    }
//
//    def "获取全部推荐课程 /api/weekrecommend/findAll"() {
//        given:
//        HttpClientUtils httpClientUtils = new HttpClientUtils()
//        when:
//        def result = httpClientUtils.doGet(Confs.env + "/api/weekrecommend/findAll", null).content
//        println result
//        def code = JSON.parse(result).get("code")
//        then:
//        assert code == 0
//    }
//
//    def "发表课程评论 /api/courseComment/save"() {
//        given:
//        HttpClientUtils httpClientUtils = new HttpClientUtils()
//        Random random = new Random();
//        int randomm = random.nextInt(10000)
//        String comment="苟利国家生死以，岂因祸福避趋之" + DateUtil.getTime()+":"+randomm
//
//        when:
//        //登录获取token
//        def token = QncqCommonParams.getToken("17601017253", "1111")
//        //保存任务
//        Map<String, String> saveParams = new HashMap<>()
//        saveParams.put("commentContent", comment)
//        saveParams.put("courseId", "2")
//        Map<String, String> findHearerMap = new HashMap()
//        findHearerMap.put("Authorization", "Bearer " + token)
//        String saveResult = httpClientUtils.doPost(Confs.env + "/api/courseComment/save", findHearerMap, saveParams).content
//        println saveResult
//        //查询任务
//        def result = httpClientUtils.doGet(Confs.env + "/api/courseComment/findByCourseId/2", findHearerMap, null).content
//        println result
//        def code = JSON.parse(result).get("code")
//        def getContent=JSON.parse(result).get("data")[0].get("content")
//        println getContent
//        then:
//        assert code == 0
//        assert getContent ==comment
//    }
    def "test3"() {
        given:
        def a = 1
        def b = 2
        when:
        def c
        c = a + b
        println "test3"
        sleep(5000)
        then:
        assert 3 == c
    }

    def "test4"() {
        given:
        def a = 1
        def b = 2
        when:
        def c
        c = a + (2 * b)
        println "test4"
        sleep(5000)
        then:
        assert 5 == c
    }
}
