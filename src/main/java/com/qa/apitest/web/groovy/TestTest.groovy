package com.qa.apitest.web.groovy

import com.alibaba.fastjson.JSON
import spock.lang.Specification
import util.HttpClientUtil

class TestTest extends Specification{
    def "111"(){
        given:
        int a=1
        int b=2
        println "111"
        when:

        int c = a+b
        then:
        assert c==3
    }
    def "weather"(){
//        http://t.weather.sojson.com/api/weather/city/101030100
        given:
        HttpClientUtil httpClientUtil =new HttpClientUtil()
        when:
        def result=httpClientUtil.doGet("t.weather.sojson.com","/api/weather/city/101030100",null,null)
        println result
        def status= JSON.parseObject(result).get("status")
        println status
        then:
        assert status==200

    }
    def "lunar"(){
        given:
        HttpClientUtil httpClientUtil =new HttpClientUtil()
        when:
        String status=null
        try {
            def result=httpClientUtil.doGet("t.weather.sojson.com","/api/weather/city/101030100",null,null)
            println result
            status= JSON.parseObject(result).get("status")
            println status
        }catch(Exception e){
            println "Exception:"+e
        }
        then:
        assert status=="300"
    }
    def "抢卷"(){
        given:
        HttpClientUtil httpClientUtil =new HttpClientUtil()
        HashMap headerMap =new HashMap()

        headerMap.put("content-type","text/plain")
        headerMap.put("referer","https://pro.m.jd.com/")
        headerMap.put("user-agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.80 Safari/537.36")
        when:
        String status=null
        try {
            def result=httpClientUtil.doPost("https://uranus.jd.com/log/m",headerMap,null,null)
            println result
            status= JSON.parseObject(result).get("c")
            println status
        }catch(Exception e){
            println "Exception:"+e
        }
        then:
        assert status=="1"
    }
}
