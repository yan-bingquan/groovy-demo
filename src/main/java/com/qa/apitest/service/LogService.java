package com.qa.apitest.service;

import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import util.FileUtil;


/**
 * @program: qncq-api-test
 * @description:
 * @author: yanbingquan
 * @create: 2019-11-27 10:30
 **/
public class LogService {
    public static void createLog(long time,Result result) {
        String dirName = "logs";
        FileUtil.createDir(dirName);

        String errorLogFileName = dirName + "/log_error_" + time + ".txt";
        FileUtil.createFile(errorLogFileName);
        //String allLogFileName = dirName + "/log_all_" + time + ".txt";
        //CreateFileUtil.writeFile(allLogFileName, result.createListener().toString());
        String totalResult = "runTestCount:" + result.getRunCount() + "\r\n" +
                "runFaileureCount:" + result.getFailureCount() + "\r\n" +
                "runIgnoreCount:" + result.getIgnoreCount() + "\r\n" +
                "RunTime:" + result.getRunTime() + "\r\n" +
                "result:" + result.wasSuccessful() + "\r\n\r\n" +
                "-----------------------------------------";

        FileUtil.writeFile(errorLogFileName, totalResult);
        for (Failure failure : result.getFailures()) {
            String errorMessage = "\r\ndescription:" + failure.getDescription() + "\r\n" +
                    "className:" + failure.getDescription().getClassName() + "\r\n" +
                    "methodName:" + failure.getDescription().getMethodName() + "\r\n" +
                    "Header:" + failure.getTestHeader() + "\r\n" +
                    "message:" + failure.getMessage() + "\r\n" +
                    "-----------------------------------------";
            //System.out.println(errorMessage);
            FileUtil.writeFile(errorLogFileName, errorMessage);
        }
    }
}