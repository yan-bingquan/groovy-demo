package util;

import javafx.util.Pair;
import org.asynchttpclient.Param;
import org.asynchttpclient.Request;
import org.asynchttpclient.Response;

import java.util.List;
import java.util.Map;

/**
 * @Author bingquan
 * @Description //TODO
 * @Date
 * @Param
 * @return
 **/
public class HttpClientUtil {
    /**
     * @param api
     * @param headerMap
     * @param params
     * @Description TODO(POST请求 ， 公共方法 ， 入参 ： url ， header ， body ， params)
     * @Param
     * @Return java.lang.String
     * @Author bingquan
     * @Date 2019-04-11
     */
    public static String doPost(String api, Map<String, String> headerMap, List<Param> params, List<Param> formParams) {
        return AsyncHttpClientUtil.doPost(api, builder -> {
            if (null != headerMap) {
                for (String key : headerMap.keySet()) {
                    builder.setHeader(key, headerMap.get(key));
                }
            }
            if (null != formParams) {
                builder.setFormParams( formParams);
                System.out.println(formParams);
            }
            if (null != params) {
                builder.setQueryParams(params);
            }
        }).getValue().getResponseBody();
    }

    /**
     * @param api
     * @param headerMap
     * @param params
     * @Description TODO(GET请求 ， 公共方法 ， 入参 ： url ， header ， params))
     * @Param
     * @Return java.lang.String
     * @Author bingquan
     * @Date 2019-04-11
     */
    public static String doGet(String env,String api, Map<String, String> headerMap, List<Param> params) {
        String url="http://"+env+api;
        Pair<Request, Response> res = AsyncHttpClientUtil.doGet(url, builder -> {
            if (null != headerMap) {
                for (String key : headerMap.keySet()) {
                    builder.setHeader(key, headerMap.get(key));
                }
            }
            builder.setQueryParams(params);
        });
        return res.getValue().getResponseBody();
    }

}